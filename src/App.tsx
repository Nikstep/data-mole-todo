import React from 'react'
import styled from 'styled-components'
import { COLORS, BORDER } from './styles/theme'
import { GlobalStyles } from './styles/GlobalStyles'
import { Input } from './components/Input'
import { TodoContextProvider } from './components/TodoContext'
import { List } from './components/List'
import { Quote } from './components/Quote'

const Container = styled.div`
  position: relative;
  height: 100vh;
  width: 100vw;
  background: ${COLORS.GREY};
`

const Wrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background: ${COLORS.WHITE};
  padding: 30px;
  border: ${BORDER.BASIC};
  max-width: 500px;
`

const App = () => (
  <>
    <GlobalStyles />
    <Container>
      <TodoContextProvider>
        <Wrapper>
          <Input />
          <List />
          <Quote />
        </Wrapper>
      </TodoContextProvider>
    </Container>
  </>
)

export default App
