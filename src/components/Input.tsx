import React, { useState, useContext } from 'react'
import styled from 'styled-components'
import { BORDER, COLORS } from '../styles/theme'
import { TodoContext } from './TodoContext'
import { ACTIONS } from '../constants/actions'

const Container = styled.form`
  display: flex;
  justify-content: space-between;
  width: 100%;
  min-width: 400px;
`

const StyledInput = styled.input`
  width: 75%;
  border: ${BORDER.BASIC};
  padding: 10px;
`

const StyledButton = styled.button`
  width: 20%;
  background: ${COLORS.BLACK};
  color: ${COLORS.WHITE};
`

export const Input = () => {
  const [value, setValue] = useState('')
  const { dispatch } = useContext(TodoContext)

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    dispatch({ type: ACTIONS.ADD_TODO, payload: value })
    setValue('')
  }

  return (
    <Container onSubmit={handleSubmit}>
      <StyledInput
        value={value}
        onChange={(e) => setValue(e.target.value)}
        name="task"
        placeholder="Enter a todo"
      />
      <StyledButton type="submit" disabled={!value}>
        Add
      </StyledButton>
    </Container>
  )
}
