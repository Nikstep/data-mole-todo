import React, { memo } from 'react'
import styled from 'styled-components'
import { BORDER } from '../styles/theme'
import { withLogger } from '../utils/withLogger'

const StyledButton = styled.button`
  opacity: 0;
  margin-left: auto;
  border: ${BORDER.BASIC};
  border-radius: 50%;
  width: 16px;
  height: 16px;
  transition: opacity 0.2s linear;
`

const StyledItem = styled.li`
  display: flex;
  align-items: center;
  padding: 10px 0;

  :hover ${StyledButton} {
    opacity: 1;
    transition: opacity 0.2s linear;
  }
`

const Text = styled.p<{ isChecked?: boolean }>`
  white-space: pre-wrap;
  margin: 0 10px;
  text-decoration: ${({ isChecked }) => isChecked && 'line-through'};
`

interface ItemProps {
  index: number
  isChecked: boolean
  todo: string
  handleRemoveTodo: (index: number) => void
  handleToggleTodo: (index: number) => void
}

export const Item: React.FC<ItemProps> = memo(
  withLogger(
    ({ index, isChecked, todo, handleRemoveTodo, handleToggleTodo }) => (
      <StyledItem key={index}>
        <input
          type="checkbox"
          checked={isChecked}
          onChange={() => handleToggleTodo(index)}
        />
        <Text isChecked={isChecked}>{todo}</Text>
        <StyledButton onClick={() => handleRemoveTodo(index)}>-</StyledButton>
      </StyledItem>
    )
  )
)
