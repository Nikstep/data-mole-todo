import React, { useContext, useCallback } from 'react'
import styled from 'styled-components'
import { TodoContext } from './TodoContext'
import { ACTIONS } from '../constants/actions'
import { Item } from './Item'
import { BORDER } from '../styles/theme'

const StyledList = styled.ul`
  display: flex;
  flex-direction: column;
  margin: 30px 0;
  border-bottom: ${BORDER.BASIC};
  padding-bottom: 20px;
`

export const List = () => {
  const { state, dispatch } = useContext(TodoContext)

  const handleRemoveTodo = useCallback((index: number) => {
    dispatch({ type: ACTIONS.REMOVE_TODO, payload: index })
  }, [])

  const handleToggleTodo = useCallback((index: number) => {
    dispatch({ type: ACTIONS.TOGGLE_TODO, payload: index })
  }, [])

  return (
    <StyledList>
      {state.length ? (
        state.map((todos, index) => (
          <Item
            {...todos}
            index={index}
            handleToggleTodo={handleToggleTodo}
            handleRemoveTodo={handleRemoveTodo}
            key={todos.todo + index}
          />
        ))
      ) : (
        <p>Your list is empty</p>
      )}
    </StyledList>
  )
}
