import React from 'react'
import { useFetch } from '../utils/useFetch'
import { API } from '../constants'

export const Quote = () => {
  const { data, error, isLoading } = useFetch(API)

  return (
    <>
      {isLoading ? (
        <p>loading...</p>
      ) : error ? (
        <p>There was an error: {error}</p>
      ) : (
        <p>Quote of the day: {data.quote}</p>
      )}
    </>
  )
}
