import React, { useEffect, useReducer, createContext, ReactNode } from 'react'
import { ACTIONS, TODOS } from '../constants/actions'
import { sortState } from '../utils/sortState'

type Store = {
  state: Todo[]
  dispatch: React.Dispatch<Action>
}

export interface Todo {
  todo: string
  isChecked: boolean
  id: number
}

interface AddTodoAction {
  type: string
  payload: string
}

interface ToggleAction {
  type: string
  payload: number
}

type Action = AddTodoAction | ToggleAction

interface TodoProps {
  children: ReactNode
}

export const TodoContext = createContext<Store>({
  state: [],
  dispatch: () => null,
})

const reducer = (state: Todo[], { type, payload }: Action): Todo[] => {
  switch (type) {
    case ACTIONS.ADD_TODO: {
      const newState = [
        { todo: payload as string, isChecked: false, id: state.length },
        ...state,
      ]
      return sortState(newState)
    }

    case ACTIONS.REMOVE_TODO:
      return state.filter((_, index) => index !== payload)

    case ACTIONS.TOGGLE_TODO:
      return sortState(
        state.map((todo, index) =>
          index === payload
            ? { ...todo, isChecked: !todo.isChecked }
            : { ...todo }
        )
      )

    default:
      return state
  }
}

const initialValue: Todo[] = JSON.parse(
  window.localStorage.getItem(TODOS) || '[]'
)

export const TodoContextProvider: React.FC<TodoProps> = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialValue)

  useEffect(() => {
    window.localStorage.setItem(TODOS, JSON.stringify(state))
  }, [state])

  return (
    <TodoContext.Provider value={{ state, dispatch }}>
      {children}
    </TodoContext.Provider>
  )
}
