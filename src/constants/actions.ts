export const ACTIONS = {
  ADD_TODO: 'addTodo',
  REMOVE_TODO: 'removeTodo',
  TOGGLE_TODO: 'toggleTodo',
}

export const TODOS = 'TODOS'
