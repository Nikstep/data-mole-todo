export const COLORS = {
  BLACK: '#030303',
  GREY: '#505050',
  WHITE: '#fff',
}

export const FONT_SIZE = {
  BASE: '1.6rem',
}

export const FONT_WEIGHT = {
  BASE: '400',
  BOLD: '700',
}

export const BORDER = {
  BASIC: `1px solid ${COLORS.BLACK}`,
}

export const MAX_CONTENT_WIDTH = '1240px'
