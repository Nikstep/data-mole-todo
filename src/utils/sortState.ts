import { Todo } from '../components/TodoContext'

export const sortState = (state: Todo[]): Todo[] => {
  state.sort((a, b) => {
    if (a.isChecked < b.isChecked) return -1
    return 0
  })
  state.sort((a, b) => {
    if (!a.isChecked && !b.isChecked && a.todo < b.todo) return -1
    return 0
  })
  return state
}
