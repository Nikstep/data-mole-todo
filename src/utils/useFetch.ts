import { useState, useEffect } from 'react'

export const useFetch = (url: string, customConfig: RequestInit = {}): any => {
  const [data, setData] = useState({})
  const [error, setError] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const config = {
    method: customConfig.body ? 'POST?' : 'GET',
    ...customConfig,
  }

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true)
      try {
        const response = await fetch(url, config)
        const json = await response.json()
        setData(json)
        setIsLoading(false)
      } catch (err) {
        setError(err)
      }
    }
    fetchData()
  }, [url])

  return { data, error, isLoading }
}
