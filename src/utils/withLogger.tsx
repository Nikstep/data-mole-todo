import React, { useEffect, useRef, ComponentType } from 'react'

export const withLogger = <P extends {}>(
  Component: ComponentType<P>
): React.FC<P> => (props) => {
  const renderCount = useRef(0)
  useEffect(() => {
    console.log('mounted')

    return () => {
      console.log('unmounted')
    }
  }, [])

  useEffect(() => {
    console.log(`rendered: ${renderCount.current++} times`)
  }, [props])

  return <Component {...(props as P)} />
}
